/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

/**
 *
 * @author edgar
 */
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import vista.*;

public class Columna1 extends Thread {
   
    VistaRelock relo;
    private Calendar calendar= Calendar.getInstance();
    Columna1(){ }
    
    public Columna1(VistaRelock relo){
        this.relo=relo;
    }
 
    
    public void cero(boolean activa0){
        relo.r1.setSelected(activa0);
        relo.r2.setSelected(activa0);
        relo.r3.setSelected(activa0);
        relo.r4.setSelected(activa0);
        relo.r5.setSelected(activa0);
        relo.r6.setSelected(activa0);
        relo.r7.setSelected(activa0);
        relo.r8.setSelected(activa0);
        relo.r9.setSelected(activa0);
        relo.r10.setSelected(activa0);
        relo.r11.setSelected(activa0);
        relo.r12.setSelected(activa0);
        relo.r13.setSelected(activa0);
        relo.r14.setSelected(activa0);
        relo.r15.setSelected(activa0);
        relo.r16.setSelected(activa0);
        relo.r17.setSelected(activa0);
        relo.r18.setSelected(activa0);
        relo.r19.setSelected(activa0);
        relo.r20.setSelected(activa0);
        relo.r21.setSelected(activa0);
        relo.r22.setSelected(activa0);
        relo.r23.setSelected(activa0);
        relo.r24.setSelected(activa0);
        relo.r25.setSelected(activa0);
        relo.r26.setSelected(activa0);
        relo.r27.setSelected(false);
        relo.r28.setSelected(false);
    }
    
    public void uno(boolean activar){
        relo.r14.setSelected(activar);
        relo.r15.setSelected(activar);
        relo.r16.setSelected(activar);
        relo.r17.setSelected(activar);
        relo.r18.setSelected(activar);
        relo.r19.setSelected(activar);
        relo.r20.setSelected(activar);
        relo.r21.setSelected(activar);
        relo.r22.setSelected(activar);
        relo.r23.setSelected(activar);
        relo.r24.setSelected(activar);
    }
    
    public void dos(boolean activar2){
        relo.r1.setSelected(activar2);
        relo.r12.setSelected(activar2);
        relo.r13.setSelected(activar2);
        relo.r14.setSelected(activar2);
        relo.r15.setSelected(activar2);
        relo.r16.setSelected(activar2);
        relo.r17.setSelected(activar2);
        relo.r18.setSelected(activar2);
        relo.r19.setSelected(activar2);
        relo.r28.setSelected(activar2);
        relo.r27.setSelected(activar2);
        relo.r6.setSelected(activar2);
        relo.r7.setSelected(activar2);
        relo.r8.setSelected(activar2);
        relo.r9.setSelected(activar2);
        relo.r10.setSelected(activar2);
        relo.r11.setSelected(activar2);
        relo.r24.setSelected(activar2);
        relo.r25.setSelected(activar2);
        relo.r26.setSelected(activar2);
    }
    
    public void tres(boolean activar3){
        relo.r1.setSelected(activar3);
        relo.r11.setSelected(activar3);
        relo.r12.setSelected(activar3);
        relo.r13.setSelected(activar3);
        relo.r14.setSelected(activar3);
        relo.r15.setSelected(activar3);
        relo.r16.setSelected(activar3);
        relo.r17.setSelected(activar3);
        relo.r18.setSelected(activar3);
        relo.r19.setSelected(activar3);
        relo.r20.setSelected(activar3);
        relo.r21.setSelected(activar3);
        relo.r22.setSelected(activar3);
        relo.r23.setSelected(activar3);
        relo.r24.setSelected(activar3);
        relo.r25.setSelected(activar3);
        relo.r26.setSelected(activar3);
        relo.r6.setSelected(activar3);
        relo.r27.setSelected(activar3);
        relo.r28.setSelected(activar3);
        
        
    }
    
    public void cuatro(boolean activa4){
        relo.r1.setSelected(activa4);
        relo.r2.setSelected(activa4);
        relo.r3.setSelected(activa4);
        relo.r4.setSelected(activa4);
        relo.r5.setSelected(activa4);
        relo.r6.setSelected(activa4);
        relo.r14.setSelected(activa4);
        relo.r15.setSelected(activa4);
        relo.r16.setSelected(activa4);
        relo.r17.setSelected(activa4);
        relo.r18.setSelected(activa4);
        relo.r19.setSelected(activa4);
        relo.r20.setSelected(activa4);
        relo.r21.setSelected(activa4);
        relo.r22.setSelected(activa4);
        relo.r23.setSelected(activa4);
        relo.r24.setSelected(activa4);
        relo.r6.setSelected(activa4);
        relo.r27.setSelected(activa4);
        relo.r28.setSelected(activa4);
    }
    
    public void cinco(boolean activa5){
        relo.r14.setSelected(activa5);
        relo.r13.setSelected(activa5);
        relo.r12.setSelected(activa5);
        relo.r1.setSelected(activa5);
        relo.r2.setSelected(activa5);
        relo.r3.setSelected(activa5);
        relo.r4.setSelected(activa5);
        relo.r5.setSelected(activa5);
        relo.r6.setSelected(activa5);
        relo.r27.setSelected(activa5);
        relo.r28.setSelected(activa5);
        relo.r19.setSelected(activa5);
        relo.r20.setSelected(activa5);
        relo.r21.setSelected(activa5);
        relo.r22.setSelected(activa5);
        relo.r23.setSelected(activa5);
        relo.r24.setSelected(activa5);
        relo.r25.setSelected(activa5);
        relo.r26.setSelected(activa5);
        relo.r11.setSelected(activa5);
    }
    
    public void seis(boolean activa6){
        relo.r1.setSelected(activa6);
        relo.r12.setSelected(activa6);
        relo.r13.setSelected(activa6);
        relo.r14.setSelected(activa6);
        relo.r2.setSelected(activa6);
        relo.r3.setSelected(activa6);
        relo.r4.setSelected(activa6);
        relo.r5.setSelected(activa6);
        relo.r6.setSelected(activa6);
        relo.r7.setSelected(activa6);
        relo.r8.setSelected(activa6);
        relo.r9.setSelected(activa6);
        relo.r10.setSelected(activa6);
        relo.r11.setSelected(activa6);
        relo.r24.setSelected(activa6);
        relo.r25.setSelected(activa6);
        relo.r26.setSelected(activa6);
        relo.r27.setSelected(activa6);
        relo.r28.setSelected(activa6);
        relo.r19.setSelected(activa6);
        relo.r20.setSelected(activa6);
        relo.r21.setSelected(activa6);
        relo.r22.setSelected(activa6);
        relo.r23.setSelected(activa6);
    }
    
    public void siete(boolean activa7){
        relo.r1.setSelected(activa7);
        relo.r12.setSelected(activa7);
        relo.r13.setSelected(activa7);
        relo.r14.setSelected(activa7);
        relo.r15.setSelected(activa7);
        relo.r16.setSelected(activa7);
        relo.r17.setSelected(activa7);
        relo.r18.setSelected(activa7);
        relo.r19.setSelected(activa7);
        relo.r20.setSelected(activa7);
        relo.r21.setSelected(activa7);
        relo.r22.setSelected(activa7);
        relo.r23.setSelected(activa7);
        relo.r24.setSelected(activa7);
    }
    
    public void ocho(boolean activa8){
         cero(activa8);
        relo.r27.setSelected(activa8);
        relo.r28.setSelected(activa8);
       
    }
    
    public void nueve(boolean activa9){
        relo.r1.setSelected(activa9);
        relo.r2.setSelected(activa9);
        relo.r3.setSelected(activa9);
        relo.r4.setSelected(activa9);
        relo.r5.setSelected(activa9);
        relo.r6.setSelected(activa9);
        relo.r11.setSelected(activa9);
        relo.r12.setSelected(activa9);
        relo.r13.setSelected(activa9);
        relo.r14.setSelected(activa9);
        relo.r15.setSelected(activa9);
        relo.r16.setSelected(activa9);
        relo.r17.setSelected(activa9);
        relo.r18.setSelected(activa9);
        relo.r19.setSelected(activa9);
        relo.r20.setSelected(activa9);
        relo.r21.setSelected(activa9);
        relo.r22.setSelected(activa9);
        relo.r23.setSelected(activa9);
        relo.r24.setSelected(activa9);
        relo.r25.setSelected(activa9);
        relo.r26.setSelected(activa9);
        relo.r27.setSelected(activa9);
        relo.r28.setSelected(activa9);
    }
    
    
    public void run(){
           String tiempo;
        while(true){
          calendar.setTimeInMillis(System.currentTimeMillis()); 
       tiempo= String.valueOf(calendar.getTime()).substring(11, 12);
       System.out.println("este es el tiempo: "+ tiempo);
        try {
            
        if(tiempo.equals("0")){
                    nueve(false);   
                    cero(true);
                    Thread.sleep(1000);
          } 
          else if(tiempo.equals("1")){ 
             cero(false);
             uno(true);
              Thread.sleep(1000);
          }
          else if(tiempo.equals("2")){
              uno(false);
              dos(true);
               Thread.sleep(1000);
          }
          else if(tiempo.equals("3")){
               dos(false);
               tres(true);
                Thread.sleep(1000);
          }
          else if(tiempo.equals("4")){
             tres(false);
             cuatro(true);
              Thread.sleep(1000);
          }
          else if(tiempo.equals("5")){
             cuatro(false);
             cinco(true);
              Thread.sleep(1000);
          }
          else if(tiempo.equals("6")){
              cinco(false);
              seis(true);
               Thread.sleep(1000);
          }
          else if(tiempo.equals("7")){
              seis(false);
              siete(true);
               Thread.sleep(1000);
          }
          else if(tiempo.equals("8")){
              siete(false);
              ocho(true);
               Thread.sleep(1000);
          }
          else if(tiempo.equals("9")){
              ocho(false);
              nueve(true);
               Thread.sleep(1000);
            }     
               
        } catch (InterruptedException ex) {
            Logger.getLogger(Columna1.class.getName()).log(Level.SEVERE, null, ex);
           }
          }        
        
                
            
        }
    
}
