/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JRadioButton;

/**
 *
 * @author edgar
 */
public class Botones extends Thread{
    
    ArrayList listaBotones=null;
    
    private JRadioButton boton1=null;
    private JRadioButton boton2=null;
    private JRadioButton boton3=null;
    private JRadioButton boton4=null;
    
    public Botones(JRadioButton boton1,JRadioButton boton2,JRadioButton boton3,JRadioButton boton4){
        this.boton1=boton1;
        this.boton2=boton2;
        this.boton3=boton3;
        this.boton4=boton4;
    }
         
    public void run(){
        while(true){
            try {
                activado(true);
                 Thread.sleep(1000);
                desactivado(false);
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(Botones.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }
    
    private void activado(boolean act){
                boton1.setSelected(act);  
                boton2.setSelected(act);  
                boton3.setSelected(act);  
                boton4.setSelected(act);  
    }
    public void desactivado(boolean des){
                boton1.setSelected(des);  
                boton2.setSelected(des);  
                boton3.setSelected(des);  
                boton4.setSelected(des);
    }
}
