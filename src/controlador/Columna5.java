/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

/**
 *
 * @author edgar
 */
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import vista.*;
public class Columna5 extends Columna4{
     private VistaRelock relo;
     private Calendar calendar= Calendar.getInstance();
     
    public Columna5(){}
    public Columna5(VistaRelock relo){
        this.relo=relo;
    }
    
    public void cero(boolean act){
        relo.r113.setSelected(act);
        relo.r114.setSelected(act);
        relo.r115.setSelected(act);
        relo.r116.setSelected(act);
        relo.r117.setSelected(act);
        relo.r118.setSelected(act);
        relo.r119.setSelected(act);
        relo.r120.setSelected(act);
        relo.r121.setSelected(act);
        relo.r122.setSelected(act);
        relo.r123.setSelected(act);
        relo.r124.setSelected(act);
        relo.r125.setSelected(act);
        relo.r126.setSelected(act);
        relo.r127.setSelected(act);
        relo.r128.setSelected(act);
        relo.r129.setSelected(act);
        relo.r130.setSelected(act);
        relo.r131.setSelected(act);
        relo.r132.setSelected(act);
        relo.r133.setSelected(act);
        relo.r135.setSelected(act);
        relo.r136.setSelected(act);
        relo.r134.setSelected(act);
        relo.r137.setSelected(act);
        relo.r138.setSelected(act);
        relo.r141.setSelected(false);
        relo.r140.setSelected(false);
    }
    
    public void uno(boolean act){
        relo.r126.setSelected(act);
        relo.r127.setSelected(act);
        relo.r128.setSelected(act);
        relo.r129.setSelected(act);
        relo.r130.setSelected(act);
        relo.r131.setSelected(act);
        relo.r132.setSelected(act);
        relo.r133.setSelected(act);
        relo.r134.setSelected(act);
        relo.r135.setSelected(act);
        relo.r136.setSelected(act);
    }
    public void dos(boolean act){
       relo.r113.setSelected(act);
       relo.r124.setSelected(act);
       relo.r125.setSelected(act);
       relo.r126.setSelected(act);
       relo.r127.setSelected(act);
       relo.r128.setSelected(act);
       relo.r129.setSelected(act);
       relo.r130.setSelected(act);
       relo.r131.setSelected(act);
       relo.r140.setSelected(act);
       relo.r141.setSelected(act);
       relo.r118.setSelected(act);
       relo.r119.setSelected(act);
       relo.r120.setSelected(act);
       relo.r121.setSelected(act);
       relo.r122.setSelected(act);
       relo.r123.setSelected(act);
       relo.r138.setSelected(act);
       relo.r137.setSelected(act);
       relo.r136.setSelected(act);
    }
    
    public void tres(boolean act){
       relo.r113.setSelected(act);
       relo.r124.setSelected(act);
       relo.r125.setSelected(act);
       relo.r126.setSelected(act);
       relo.r127.setSelected(act);
       relo.r128.setSelected(act);
       relo.r129.setSelected(act);
       relo.r130.setSelected(act);
       relo.r131.setSelected(act);
       relo.r132.setSelected(act);
       relo.r133.setSelected(act);
       relo.r134.setSelected(act);
       relo.r135.setSelected(act);
       relo.r136.setSelected(act);
       relo.r137.setSelected(act);
       relo.r138.setSelected(act);
       relo.r123.setSelected(act);
       relo.r118.setSelected(act);
       relo.r140.setSelected(act);
       relo.r141.setSelected(act);
    }
    
    public void cuatro(boolean act){
       relo.r113.setSelected(act);    
       relo.r114.setSelected(act);
       relo.r115.setSelected(act);
       relo.r116.setSelected(act);
       relo.r117.setSelected(act);
       relo.r118.setSelected(act);
       relo.r140.setSelected(act);
       relo.r141.setSelected(act);
       relo.r126.setSelected(act);
       relo.r127.setSelected(act);
       relo.r128.setSelected(act);
       relo.r129.setSelected(act);
       relo.r130.setSelected(act);
       relo.r131.setSelected(act);
       relo.r132.setSelected(act);
       relo.r133.setSelected(act);
       relo.r134.setSelected(act);
       relo.r135.setSelected(act);
       relo.r136.setSelected(act);
    }
    public void cinco(boolean act){
       relo.r113.setSelected(act);
       relo.r124.setSelected(act);
       relo.r125.setSelected(act);
       relo.r126.setSelected(act);    
       relo.r114.setSelected(act);
       relo.r115.setSelected(act);
       relo.r116.setSelected(act);
       relo.r117.setSelected(act);
       relo.r118.setSelected(act);
       relo.r140.setSelected(act);
       relo.r141.setSelected(act);
       relo.r131.setSelected(act);
       relo.r132.setSelected(act);
       relo.r133.setSelected(act);
       relo.r134.setSelected(act);
       relo.r135.setSelected(act);
       relo.r136.setSelected(act);
       relo.r137.setSelected(act);
       relo.r138.setSelected(act);
       relo.r123.setSelected(act);
    }
    
    public void seis(boolean act){
       relo.r113.setSelected(act);
       relo.r124.setSelected(act);
       relo.r125.setSelected(act);
       relo.r126.setSelected(act);
       relo.r114.setSelected(act);
       relo.r115.setSelected(act);
       relo.r116.setSelected(act);
       relo.r117.setSelected(act);
       relo.r118.setSelected(act);
       relo.r119.setSelected(act);
       relo.r120.setSelected(act);
       relo.r121.setSelected(act);
       relo.r122.setSelected(act);
       relo.r123.setSelected(act);
       relo.r131.setSelected(act);
       relo.r132.setSelected(act);
       relo.r133.setSelected(act);
       relo.r134.setSelected(act);
       relo.r135.setSelected(act);
       relo.r136.setSelected(act);
       relo.r137.setSelected(act);
       relo.r138.setSelected(act);
       relo.r140.setSelected(act);
       relo.r141.setSelected(act);
    }
    
    public void siete(boolean act){
       relo.r113.setSelected(act);
       relo.r124.setSelected(act);
       relo.r125.setSelected(act);
       relo.r126.setSelected(act);
       relo.r127.setSelected(act);
       relo.r128.setSelected(act);
       relo.r129.setSelected(act);
       relo.r130.setSelected(act);
       relo.r131.setSelected(act);
       relo.r132.setSelected(act);
       relo.r133.setSelected(act);
       relo.r134.setSelected(act);
       relo.r135.setSelected(act);
       relo.r136.setSelected(act);
    }
       
    public void ocho(boolean act){
        cero(act);
        relo.r140.setSelected(act);
        relo.r141.setSelected(act);
    }
    
    public void nueve(boolean act){
        relo.r113.setSelected(act);
        relo.r114.setSelected(act);
        relo.r115.setSelected(act);
        relo.r116.setSelected(act);
        relo.r117.setSelected(act);
        relo.r118.setSelected(act);
        relo.r140.setSelected(act);
        relo.r141.setSelected(act);
        relo.r126.setSelected(act);
        relo.r127.setSelected(act);
        relo.r128.setSelected(act);
        relo.r129.setSelected(act);
        relo.r130.setSelected(act);
        relo.r131.setSelected(act);
        relo.r124.setSelected(act);
        relo.r125.setSelected(act);
        relo.r132.setSelected(act);
        relo.r133.setSelected(act);
        relo.r134.setSelected(act);
        relo.r135.setSelected(act);
        relo.r136.setSelected(act);
        relo.r137.setSelected(act);
        relo.r138.setSelected(act);
        relo.r123.setSelected(act);
    }
    
    
     public void run(){
         String tiempo;
    while(true){
         calendar.setTimeInMillis(System.currentTimeMillis()); 
             tiempo= String.valueOf(calendar.get(Calendar.SECOND));
             System.out.println(calendar.get(Calendar.SECOND));
         try {
             if(tiempo.substring(1).equals("")){
                    nueve(false);   
                    cero(true);
                    Thread.sleep(1000);
          } 
          else if(tiempo.substring(0,1).equals("1")){ 
             cero(false);
             uno(true);
              Thread.sleep(1000);
          }
          else if(tiempo.substring(0,1).equals("2")){
              uno(false);
              dos(true);
               Thread.sleep(1000);
          }
          else if(tiempo.substring(0,1).equals("3")){
               dos(false);
               tres(true);
                Thread.sleep(1000);
          }
          else if(tiempo.substring(0,1).equals("4")){
             tres(false);
             cuatro(true);
              Thread.sleep(1000);
          }
          else if(tiempo.substring(0,1).equals("5")){
             cuatro(false);
             cinco(true);
              Thread.sleep(1000);
          }
          else if(tiempo.substring(0,1).equals("6")){
              cinco(false);
              seis(true);
               Thread.sleep(1000);
          }
          else if(tiempo.substring(0,1).equals("7")){
              seis(false);
              siete(true);
               Thread.sleep(1000);
          }
          else if(tiempo.substring(0,1).equals("8")){
              siete(false);
              ocho(true);
               Thread.sleep(1000);
          }
          else if(tiempo.substring(0,1).equals("9")){
              ocho(false);
              nueve(true);
               Thread.sleep(1000);
            }          
               
        } catch (InterruptedException ex) {Logger.getLogger(Columna1.class.getName()).log(Level.SEVERE, null, ex);}
                
           }      
     }
}
