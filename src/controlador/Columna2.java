/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

/**
 *
 * @author edgar
 */
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import vista.VistaRelock;
public class Columna2 extends Columna1{
    
    private VistaRelock relo;
    
     private Calendar calendar= Calendar.getInstance();
    
    
    public Columna2(){}
    public Columna2(VistaRelock relo){
        this.relo=relo;
    }
    
    public void cero(boolean act){
       relo.r29.setSelected(act);    
       relo.r30.setSelected(act);
       relo.r31.setSelected(act);
       relo.r32.setSelected(act);
       relo.r33.setSelected(act);
       relo.r34.setSelected(act);
       relo.r35.setSelected(act);
       relo.r36.setSelected(act);
       relo.r37.setSelected(act);
       relo.r38.setSelected(act);
       relo.r39.setSelected(act);
       relo.r40.setSelected(act);
       relo.r41.setSelected(act);
       relo.r42.setSelected(act);
       relo.r43.setSelected(act);
       relo.r44.setSelected(act);
       relo.r45.setSelected(act);
       relo.r46.setSelected(act);
       relo.r47.setSelected(act);
       relo.r48.setSelected(act);
       relo.r49.setSelected(act);
       relo.r50.setSelected(act);
       relo.r51.setSelected(act);
       relo.r52.setSelected(act);
       relo.r53.setSelected(act);
       relo.r54.setSelected(act);
       relo.r55.setSelected(false);
       relo.r56.setSelected(false);
    }
    
    public void uno(boolean act){
       relo.r42.setSelected(act);    
       relo.r43.setSelected(act);
       relo.r44.setSelected(act);
       relo.r45.setSelected(act);
       relo.r46.setSelected(act);
       relo.r47.setSelected(act);
       relo.r48.setSelected(act);
       relo.r49.setSelected(act);
       relo.r50.setSelected(act);
       relo.r51.setSelected(act);
       relo.r52.setSelected(act);
    }
    public void dos(boolean act){
       relo.r29.setSelected(act);    
       relo.r40.setSelected(act);
       relo.r41.setSelected(act);
       relo.r42.setSelected(act);
       relo.r43.setSelected(act);
       relo.r44.setSelected(act);
       relo.r45.setSelected(act);
       relo.r46.setSelected(act);
       relo.r47.setSelected(act);
       relo.r56.setSelected(act);
       relo.r55.setSelected(act);
       relo.r34.setSelected(act);
       relo.r35.setSelected(act);
       relo.r36.setSelected(act);
       relo.r37.setSelected(act);
       relo.r38.setSelected(act);
       relo.r39.setSelected(act);
       relo.r54.setSelected(act);
       relo.r53.setSelected(act);
       relo.r52.setSelected(act);
    }
    
    public void tres(boolean act){
        relo.r29.setSelected(act);
        relo.r40.setSelected(act);
        relo.r41.setSelected(act);
        relo.r42.setSelected(act);
        relo.r43.setSelected(act);
        relo.r44.setSelected(act);
       relo.r45.setSelected(act);
       relo.r46.setSelected(act);
       relo.r47.setSelected(act);
       relo.r48.setSelected(act);
       relo.r49.setSelected(act);
       relo.r50.setSelected(act);
       relo.r51.setSelected(act);
       relo.r52.setSelected(act);
       relo.r53.setSelected(act);
       relo.r54.setSelected(act);
       relo.r55.setSelected(act);
       relo.r56.setSelected(act);
       relo.r34.setSelected(act);
       relo.r39.setSelected(act);
       
    }
    
    public void cuatro(boolean act){
       relo.r29.setSelected(act);    
       relo.r30.setSelected(act);
       relo.r31.setSelected(act);
       relo.r32.setSelected(act);
       relo.r33.setSelected(act);
       relo.r34.setSelected(act);
       relo.r42.setSelected(act);
       relo.r43.setSelected(act);
       relo.r44.setSelected(act);
       relo.r45.setSelected(act);
       relo.r46.setSelected(act);
       relo.r47.setSelected(act);
       relo.r48.setSelected(act);
       relo.r49.setSelected(act);
       relo.r50.setSelected(act);
       relo.r51.setSelected(act);
       relo.r52.setSelected(act);
       relo.r55.setSelected(act);
       relo.r56.setSelected(act);
    }
    public void cinco(boolean act){
       relo.r42.setSelected(act);
       relo.r41.setSelected(act);
       relo.r40.setSelected(act);
       relo.r29.setSelected(act);
       relo.r30.setSelected(act);
       relo.r31.setSelected(act);
       relo.r32.setSelected(act);
       relo.r33.setSelected(act);
       relo.r34.setSelected(act);
       relo.r55.setSelected(act);
       relo.r56.setSelected(act);
       relo.r47.setSelected(act);
       relo.r48.setSelected(act);
       relo.r49.setSelected(act);
       relo.r50.setSelected(act);
       relo.r51.setSelected(act);
       relo.r52.setSelected(act);
       relo.r53.setSelected(act);
       relo.r54.setSelected(act);
       relo.r39.setSelected(act);
    }
    
    public void seis(boolean act){
       relo.r42.setSelected(act);
       relo.r41.setSelected(act);
       relo.r40.setSelected(act);
       relo.r29.setSelected(act);
       relo.r30.setSelected(act);
       relo.r31.setSelected(act);
       relo.r32.setSelected(act);
       relo.r33.setSelected(act);
       relo.r34.setSelected(act);
       relo.r35.setSelected(act);
       relo.r36.setSelected(act);
       relo.r37.setSelected(act);
       relo.r38.setSelected(act);
       relo.r39.setSelected(act);
       relo.r54.setSelected(act);
       relo.r53.setSelected(act);
       relo.r52.setSelected(act);
       relo.r51.setSelected(act);
       relo.r50.setSelected(act);
       relo.r49.setSelected(act);
       relo.r48.setSelected(act);
       relo.r47.setSelected(act);
       relo.r56.setSelected(act);
       relo.r55.setSelected(act);
        
    }
    
    public void siete(boolean act){
       relo.r29.setSelected(act);    
       relo.r40.setSelected(act);
       relo.r41.setSelected(act);
       relo.r42.setSelected(act);
       relo.r43.setSelected(act);
       relo.r44.setSelected(act);
       relo.r45.setSelected(act);
       relo.r46.setSelected(act);
       relo.r47.setSelected(act);
       relo.r48.setSelected(act);
       relo.r49.setSelected(act);
       relo.r50.setSelected(act);
       relo.r51.setSelected(act);
       relo.r52.setSelected(act);
    }
       
    public void ocho(boolean act){
        cero(act);
        relo.r55.setSelected(act);
        relo.r56.setSelected(act);
    }
    
    public void nueve(boolean act){
       relo.r29.setSelected(act);    
       relo.r40.setSelected(act);
       relo.r41.setSelected(act);
       relo.r42.setSelected(act);
       relo.r43.setSelected(act);
       relo.r44.setSelected(act);
       relo.r45.setSelected(act);
       relo.r46.setSelected(act);
       relo.r47.setSelected(act);
       relo.r30.setSelected(act);
       relo.r31.setSelected(act);
       relo.r32.setSelected(act);
       relo.r33.setSelected(act);
       relo.r34.setSelected(act);
       relo.r48.setSelected(act);
       relo.r49.setSelected(act);
       relo.r50.setSelected(act);
       relo.r51.setSelected(act);
       relo.r52.setSelected(act);
       relo.r53.setSelected(act);
       relo.r54.setSelected(act);
       relo.r55.setSelected(act);
       relo.r56.setSelected(act);
       relo.r39.setSelected(act);
    }
    
     public void run(){
        String tiempo=null;    
            
        while(true){
          calendar.setTimeInMillis(System.currentTimeMillis()); 
       tiempo= String.valueOf(calendar.getTime()).substring(12, 13);
        try {
            
        if(tiempo.equals("")){
                    nueve(false);   
                    cero(true);
                    Thread.sleep(1000);
          } 
          else if(tiempo.equals("1")){ 
             cero(false);
             uno(true);
              Thread.sleep(1000);
          }
          else if(tiempo.equals("2")){
              uno(false);
              dos(true);
               Thread.sleep(1000);
          }
          else if(tiempo.equals("3")){
               dos(false);
               tres(true);
                Thread.sleep(1000);
          }
          else if(tiempo.equals("4")){
             tres(false);
             cuatro(true);
              Thread.sleep(1000);
          }
          else if(tiempo.equals("5")){
             cuatro(false);
             cinco(true);
              Thread.sleep(1000);
          }
          else if(tiempo.equals("6")){
              cinco(false);
              seis(true);
               Thread.sleep(1000);
          }
          else if(tiempo.equals("7")){
              seis(false);
              siete(true);
               Thread.sleep(1000);
          }
          else if(tiempo.equals("8")){
              siete(false);
              ocho(true);
               Thread.sleep(1000);
          }
          else if(tiempo.equals("9")){
              ocho(false);
              nueve(true);
               Thread.sleep(1000);
            }     
               
        } catch (InterruptedException ex) {
            Logger.getLogger(Columna1.class.getName()).log(Level.SEVERE, null, ex);
           }
        }
     }
}
