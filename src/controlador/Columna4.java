/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

/**
 *
 * @author edgar
 */
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import vista.*;
public class Columna4 extends Columna3{
    
    private VistaRelock relo;
    
     private Calendar calendar= Calendar.getInstance();
    
    public Columna4(){}
    public Columna4(VistaRelock relo){
        this.relo=relo;
    }
    
    public void cero(boolean act){
        relo.r85.setSelected(act);
        relo.r86.setSelected(act);
        relo.r87.setSelected(act);
        relo.r88.setSelected(act);
        relo.r89.setSelected(act);
        relo.r90.setSelected(act);
        relo.r91.setSelected(act);
        relo.r92.setSelected(act);
        relo.r94.setSelected(act);
        relo.r93.setSelected(act);
        relo.r95.setSelected(act);
        relo.r96.setSelected(act);
        relo.r97.setSelected(act);
        relo.r98.setSelected(act);
        relo.r99.setSelected(act);
        relo.r100.setSelected(act);
        relo.r101.setSelected(act);
        relo.r102.setSelected(act);
        relo.r103.setSelected(act);
        relo.r104.setSelected(act);
        relo.r105.setSelected(act);
        relo.r106.setSelected(act);
        relo.r107.setSelected(act);
        relo.r108.setSelected(act);
        relo.r109.setSelected(act);
        relo.r110.setSelected(act);
        relo.r111.setSelected(false);
        relo.r112.setSelected(false);
    }
    
    public void uno(boolean act){
        relo.r98.setSelected(act);
        relo.r99.setSelected(act);
        relo.r100.setSelected(act);
        relo.r101.setSelected(act);
        relo.r102.setSelected(act);
        relo.r103.setSelected(act);
        relo.r104.setSelected(act);
        relo.r105.setSelected(act);
        relo.r106.setSelected(act);
        relo.r107.setSelected(act);
        relo.r108.setSelected(act);
    }
    public void dos(boolean act){
       relo.r85.setSelected(act);
       relo.r96.setSelected(act);
       relo.r97.setSelected(act);
       relo.r98.setSelected(act);
       relo.r99.setSelected(act);
       relo.r100.setSelected(act);
       relo.r101.setSelected(act);
       relo.r102.setSelected(act);
       relo.r103.setSelected(act);
       relo.r112.setSelected(act);
       relo.r111.setSelected(act);
       relo.r90.setSelected(act);
       relo.r91.setSelected(act);
       relo.r92.setSelected(act);
       relo.r93.setSelected(act);
       relo.r94.setSelected(act);
       relo.r95.setSelected(act);
       relo.r110.setSelected(act);
       relo.r109.setSelected(act);
       relo.r108.setSelected(act);
    }
    
    public void tres(boolean act){
       relo.r85.setSelected(act);
       relo.r96.setSelected(act);
       relo.r97.setSelected(act);
       relo.r98.setSelected(act);
       relo.r99.setSelected(act);
       relo.r100.setSelected(act);
       relo.r101.setSelected(act);
       relo.r102.setSelected(act);
       relo.r103.setSelected(act);
       relo.r112.setSelected(act);
       relo.r111.setSelected(act);
       relo.r104.setSelected(act);
       relo.r105.setSelected(act);
       relo.r106.setSelected(act);
       relo.r107.setSelected(act);
       relo.r108.setSelected(act);
       relo.r109.setSelected(act);
       relo.r110.setSelected(act);
       relo.r95.setSelected(act);
       relo.r90.setSelected(act);
       relo.r111.setSelected(act);
       relo.r112.setSelected(act);
    }
    
    public void cuatro(boolean act){
       relo.r85.setSelected(act);    
       relo.r86.setSelected(act);
       relo.r87.setSelected(act);
       relo.r88.setSelected(act);
       relo.r89.setSelected(act);
       relo.r90.setSelected(act);
       relo.r111.setSelected(act);
       relo.r112.setSelected(act);
       relo.r98.setSelected(act);
       relo.r99.setSelected(act);
       relo.r100.setSelected(act);
       relo.r101.setSelected(act);
       relo.r102.setSelected(act);
       relo.r103.setSelected(act);
       relo.r104.setSelected(act);
       relo.r105.setSelected(act);
       relo.r106.setSelected(act);
       relo.r107.setSelected(act);
       relo.r108.setSelected(act);
    }
    public void cinco(boolean act){
       relo.r85.setSelected(act);
       relo.r96.setSelected(act);
       relo.r97.setSelected(act);
       relo.r98.setSelected(act);   
       relo.r86.setSelected(act);
       relo.r87.setSelected(act);
       relo.r88.setSelected(act);
       relo.r89.setSelected(act);
       relo.r90.setSelected(act);
       relo.r111.setSelected(act);
       relo.r112.setSelected(act);
       relo.r103.setSelected(act);
       relo.r104.setSelected(act);
       relo.r105.setSelected(act);
       relo.r106.setSelected(act);
       relo.r107.setSelected(act);
       relo.r108.setSelected(act);
       relo.r109.setSelected(act);
       relo.r110.setSelected(act);
       relo.r95.setSelected(act);
      }
    
    public void seis(boolean act){
       relo.r98.setSelected(act);    
       relo.r97.setSelected(act);
       relo.r96.setSelected(act);
       relo.r85.setSelected(act);
       relo.r86.setSelected(act);
       relo.r87.setSelected(act);
       relo.r88.setSelected(act);
       relo.r89.setSelected(act);
       relo.r90.setSelected(act);
       relo.r111.setSelected(act);
       relo.r112.setSelected(act);
       relo.r103.setSelected(act);
       relo.r104.setSelected(act);
       relo.r105.setSelected(act);
       relo.r106.setSelected(act);
       relo.r107.setSelected(act);
       relo.r108.setSelected(act);
       relo.r109.setSelected(act);
       relo.r110.setSelected(act);
       relo.r91.setSelected(act);
       relo.r92.setSelected(act);
       relo.r93.setSelected(act);
       relo.r94.setSelected(act);
       relo.r95.setSelected(act);
    }
    
    public void siete(boolean act){
        relo.r85.setSelected(act);
        relo.r96.setSelected(act);
        relo.r97.setSelected(act);
        relo.r98.setSelected(act);
        relo.r99.setSelected(act);
        relo.r100.setSelected(act);
        relo.r101.setSelected(act);
        relo.r102.setSelected(act);
        relo.r103.setSelected(act);
        relo.r104.setSelected(act);
        relo.r105.setSelected(act);
        relo.r106.setSelected(act);
        relo.r107.setSelected(act);
        relo.r108.setSelected(act);
    }
       
    public void ocho(boolean act){
        cero(act);
        relo.r111.setSelected(act);
        relo.r112.setSelected(act);
    }
    
    public void nueve(boolean act){
        relo.r85.setSelected(act);
        relo.r96.setSelected(act);
        relo.r97.setSelected(act);
        relo.r98.setSelected(act);
        relo.r99.setSelected(act);
        relo.r100.setSelected(act);
        relo.r101.setSelected(act);
        relo.r102.setSelected(act);
        relo.r103.setSelected(act);
        relo.r86.setSelected(act);
        relo.r87.setSelected(act);
        relo.r88.setSelected(act);
        relo.r89.setSelected(act);
        relo.r90.setSelected(act);
        relo.r111.setSelected(act);
        relo.r112.setSelected(act);
        relo.r104.setSelected(act);
        relo.r105.setSelected(act);
        relo.r106.setSelected(act);
        relo.r107.setSelected(act);
        relo.r108.setSelected(act);
        relo.r109.setSelected(act);
        relo.r110.setSelected(act);
        relo.r95.setSelected(act);
    }
    
    
     public void run(){
            String tiempo;
       while(true){
            calendar.setTimeInMillis(System.currentTimeMillis()); 
            tiempo= String.valueOf(calendar.getTime()).substring(15, 16);
            System.out.println(tiempo);
        try {
            if(tiempo.equals("0")){
                    nueve(false);   
                    cero(true);
                    Thread.sleep(1000);
          } 
          else if(tiempo.equals("1")){ 
             cero(false);
             uno(true);
              Thread.sleep(1000);
          }
          else if(tiempo.equals("2")){
              uno(false);
              dos(true);
               Thread.sleep(1000);
          }
          else if(tiempo.equals("3")){
               dos(false);
               tres(true);
                Thread.sleep(1000);
          }
          else if(tiempo.equals("4")){
             tres(false);
             cuatro(true);
              Thread.sleep(1000);
          }
          else if(tiempo.equals("5")){
             cuatro(false);
             cinco(true);
              Thread.sleep(1000);
          }
          else if(tiempo.equals("6")){
              cinco(false);
              seis(true);
               Thread.sleep(1000);
          }
          else if(tiempo.equals("7")){
              seis(false);
              siete(true);
               Thread.sleep(1000);
          }
          else if(tiempo.equals("8")){
              siete(false);
              ocho(true);
               Thread.sleep(1000);
          }
          else if(tiempo.equals("9")){
              ocho(false);
              nueve(true);
               Thread.sleep(1000);
            }     
               
        } catch (InterruptedException ex) {
            Logger.getLogger(Columna1.class.getName()).log(Level.SEVERE, null, ex);
        }
       }        
            
        }
    
}
