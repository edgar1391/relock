/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

/**
 *
 * @author edgar
 */
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import vista.*;
public class Columna6 extends Columna5 {
    
    private VistaRelock relo;
    private Calendar calendar= Calendar.getInstance();
    
    
    public Columna6(){}
    public Columna6(VistaRelock relo){
        this.relo=relo;
    }
    
    public void cero(boolean act){
        relo.r142.setSelected(act);
        relo.r143.setSelected(act);
        relo.r144.setSelected(act);
        relo.r145.setSelected(act);
        relo.r146.setSelected(act);
        relo.r147.setSelected(act);
        relo.r148.setSelected(act);
        relo.r149.setSelected(act);
        relo.r150.setSelected(act);
        relo.r151.setSelected(act);
        relo.r152.setSelected(act);
        relo.r153.setSelected(act);
        relo.r154.setSelected(act);
        relo.r155.setSelected(act);
        relo.r156.setSelected(act);
        relo.r157.setSelected(act);
        relo.r158.setSelected(act);
        relo.r159.setSelected(act);
        relo.r160.setSelected(act);
        relo.r161.setSelected(act);
        relo.r162.setSelected(act);
        relo.r163.setSelected(act);
        relo.r164.setSelected(act);
        relo.r165.setSelected(act);
        relo.r166.setSelected(act);
        relo.r167.setSelected(act);
        relo.r168.setSelected(false);
        relo.r169.setSelected(false);
    }
    
    public void uno(boolean act){
        relo.r155.setSelected(act);
        relo.r156.setSelected(act);
        relo.r157.setSelected(act);
        relo.r158.setSelected(act);
        relo.r159.setSelected(act);
        relo.r160.setSelected(act);
        relo.r161.setSelected(act);
        relo.r162.setSelected(act);
        relo.r163.setSelected(act);
        relo.r164.setSelected(act);
        relo.r165.setSelected(act);
    }
    public void dos(boolean act){
       relo.r142.setSelected(act);
       relo.r153.setSelected(act);
       relo.r154.setSelected(act);
       relo.r155.setSelected(act);
       relo.r156.setSelected(act);
       relo.r157.setSelected(act);
       relo.r158.setSelected(act);
       relo.r159.setSelected(act);
       relo.r160.setSelected(act);
       relo.r169.setSelected(act);
       relo.r168.setSelected(act);
       relo.r147.setSelected(act);
       relo.r148.setSelected(act);
       relo.r149.setSelected(act);
       relo.r150.setSelected(act);
       relo.r151.setSelected(act);
       relo.r152.setSelected(act);
       relo.r165.setSelected(act);
       relo.r166.setSelected(act);
       relo.r167.setSelected(act);
    }
    
    public void tres(boolean act){
       relo.r142.setSelected(act);
       relo.r152.setSelected(act);
       relo.r153.setSelected(act);
       relo.r154.setSelected(act);
       relo.r155.setSelected(act);
       relo.r156.setSelected(act);
       relo.r157.setSelected(act);
       relo.r158.setSelected(act);
       relo.r159.setSelected(act);
       relo.r160.setSelected(act);
       relo.r161.setSelected(act);
       relo.r162.setSelected(act);
       relo.r163.setSelected(act);
       relo.r164.setSelected(act);
       relo.r165.setSelected(act);
       relo.r166.setSelected(act);
       relo.r167.setSelected(act);
       relo.r132.setSelected(act);
       relo.r169.setSelected(act);
       relo.r168.setSelected(act);
       relo.r147.setSelected(act);
    }
    
    public void cuatro(boolean act){
       relo.r142.setSelected(act);    
       relo.r143.setSelected(act);
       relo.r144.setSelected(act);
       relo.r145.setSelected(act);
       relo.r146.setSelected(act);
       relo.r147.setSelected(act);
       relo.r155.setSelected(act);
       relo.r156.setSelected(act);
       relo.r157.setSelected(act);
       relo.r158.setSelected(act);
       relo.r159.setSelected(act);
       relo.r160.setSelected(act);
       relo.r161.setSelected(act);
       relo.r162.setSelected(act);
       relo.r163.setSelected(act);
       relo.r164.setSelected(act);
       relo.r165.setSelected(act);
       relo.r168.setSelected(act);
       relo.r169.setSelected(act);
    }
    public void cinco(boolean act){
        
       relo.r142.setSelected(act);
       relo.r153.setSelected(act);
       relo.r154.setSelected(act);
       relo.r155.setSelected(act);    
       relo.r143.setSelected(act);
       relo.r144.setSelected(act);
       relo.r145.setSelected(act);
       relo.r146.setSelected(act);
       relo.r147.setSelected(act);
       relo.r168.setSelected(act);
       relo.r169.setSelected(act);
       relo.r160.setSelected(act);
       relo.r161.setSelected(act);
       relo.r162.setSelected(act);
       relo.r163.setSelected(act);
       relo.r164.setSelected(act);
       relo.r165.setSelected(act);
       relo.r166.setSelected(act);
       relo.r167.setSelected(act);
       relo.r152.setSelected(act);
    }
    
    public void seis(boolean act){
       relo.r142.setSelected(act);
       relo.r153.setSelected(act);
       relo.r154.setSelected(act);
       relo.r155.setSelected(act);
       relo.r143.setSelected(act);
       relo.r144.setSelected(act);
       relo.r145.setSelected(act);
       relo.r146.setSelected(act);
       relo.r147.setSelected(act);
       relo.r148.setSelected(act);
       relo.r149.setSelected(act);
       relo.r150.setSelected(act);
       relo.r151.setSelected(act);
       relo.r152.setSelected(act);
       relo.r160.setSelected(act);
       relo.r161.setSelected(act);
       relo.r162.setSelected(act);
       relo.r163.setSelected(act);
       relo.r164.setSelected(act);
       relo.r165.setSelected(act);
       relo.r166.setSelected(act);
       relo.r167.setSelected(act);
       relo.r168.setSelected(act);
       relo.r169.setSelected(act);
    }
    
    public void siete(boolean act){
       relo.r142.setSelected(act);
       relo.r153.setSelected(act);
       relo.r154.setSelected(act);
       relo.r155.setSelected(act);
       relo.r156.setSelected(act);
       relo.r157.setSelected(act);
       relo.r158.setSelected(act);
       relo.r159.setSelected(act);
       relo.r160.setSelected(act);
       relo.r161.setSelected(act);
       relo.r162.setSelected(act);
       relo.r163.setSelected(act);
       relo.r164.setSelected(act);
       relo.r165.setSelected(act);
    }
       
    public void ocho(boolean act){
        cero(act);
        relo.r168.setSelected(act);
        relo.r169.setSelected(act);
    }
    
    public void nueve(boolean act){
        relo.r142.setSelected(act);
        relo.r153.setSelected(act);
        relo.r154.setSelected(act);
        relo.r155.setSelected(act);
        relo.r156.setSelected(act);
        relo.r157.setSelected(act);
        relo.r158.setSelected(act);
        relo.r159.setSelected(act);
        relo.r160.setSelected(act);
        relo.r168.setSelected(act);
        relo.r169.setSelected(act);
        relo.r129.setSelected(act);
        relo.r161.setSelected(act);
        relo.r162.setSelected(act);
        relo.r163.setSelected(act);
        relo.r164.setSelected(act);
        relo.r165.setSelected(act);
        relo.r166.setSelected(act);
        relo.r167.setSelected(act);
        relo.r152.setSelected(act);
        relo.r143.setSelected(act);
        relo.r144.setSelected(act);
        relo.r145.setSelected(act);
        relo.r146.setSelected(act);
        relo.r147.setSelected(act);
    }
    
     public void run(){
            String tiempo;       
     while(true){
            calendar.setTimeInMillis(System.currentTimeMillis()); 
             tiempo= String.valueOf(calendar.get(Calendar.SECOND));
           
           try { 
               
           
          if(tiempo.substring(1).equals("0") || tiempo.substring(0).equals("0") ){
                    nueve(false);   
                    cero(true);
                   Thread.sleep(1000);
                
          } 
          else if(tiempo.substring(1).equals("1") || tiempo.substring(0).equals("1")){ 
             cero(false);
             uno(true);
              Thread.sleep(1000);
          }
          else if(tiempo.substring(1).equals("2") || tiempo.substring(0).equals("2")){
              uno(false);
              dos(true);
               Thread.sleep(1000);
          }
          else if(tiempo.substring(1).equals("3") || tiempo.substring(0).equals("3")){
               dos(false);
               tres(true);
                Thread.sleep(1000);
          }
          else if(tiempo.substring(1).equals("4") || tiempo.substring(0).equals("4")){
             tres(false);
             cuatro(true);
              Thread.sleep(1000);
          }
          else if(tiempo.substring(1).equals("5") || tiempo.substring(0).equals("5")){
             cuatro(false);
             cinco(true);
              Thread.sleep(1000);
          }
          else if(tiempo.substring(1).equals("6") || tiempo.substring(0).equals("6")){
              cinco(false);
              seis(true);
               Thread.sleep(1000);
          }
          else if(tiempo.substring(1).equals("7") || tiempo.substring(0).equals("7")){
              seis(false);
              siete(true);
               Thread.sleep(1000);
          }
          else if(tiempo.substring(1).equals("8") || tiempo.substring(0).equals("8")){
              siete(false);
              ocho(true);
               Thread.sleep(1000);
          }
          else if(tiempo.substring(1).equals("9") || tiempo.substring(0).equals("9")){
              ocho(false);
              nueve(true);
               Thread.sleep(1000);
            }
          }catch (InterruptedException ex) {
                 Logger.getLogger(Columna6.class.getName()).log(Level.SEVERE, null, ex);
           }
           
       }
        
      }
}
